import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Providers
 */
import { ApiServiceProvider } from './../../providers/api-service/api-service';

@Component({
  selector: 'page-users-list',
  templateUrl: 'users-list.html',
})

export class UsersListPage {

  users: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private apiService: ApiServiceProvider) {
    this.initializeUsers();
  }

  initializeUsers() {
      this.apiService.listingResource()
      .then((result) => {  
      this.users = result;
    });
  }

  selectedItem(id) {
    console.log("ID: "+id);
  }

  //Limpar código
  getUsers(ev: any) {

    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.users = this.users.filter((user) => {
        return (user.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
    else
      this.initializeUsers();
  }
  
}
