import { Component } from '@angular/core';
/**
 * Providers
 */
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ApiServiceProvider } from './../../providers/api-service/api-service';

@Component({
  selector: 'page-user-create',
  templateUrl
  : 'user-create.html',
})

export class UserCreatePage {

  private userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private api: ApiServiceProvider ) {
    this.userValidation();
  }

  createUser() {
    this.api.creatingResource(this.userForm.value)
    .then((result) => {console.log(result)})
    .catch((err) => console.log("Deu ruim!!"));
  }

  userValidation() {
    this.userForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'user': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'mail': ['', Validators.compose([Validators.required, Validators.email])],
      'id': ['', Validators.required]
    });
  }

}
