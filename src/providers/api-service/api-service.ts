import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class ApiServiceProvider {

  url = 'http://jsonplaceholder.typicode.com/users/';
  data: any;

 constructor(public http: Http) {}

  listingResource() {
    return new Promise(resolve => {
      this.http.get(this.url)
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      })
    });
  }

  creatingResource(obj) {
    return new Promise(resolve => {
      this.http.post(this.url, obj)
      .map((res) => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(data);
      })
    })
  }

  updatingResource(id, obj) {
    return new Promise(resolve => {
      this.http.post(this.url + id, obj)
      .map((res) => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(data);
      })
    })
  }

  deletingResource() {}

}
